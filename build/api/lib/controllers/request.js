"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiRequest = exports.BaseHttpRequest = void 0;
const got_1 = __importDefault(require("got"));
class BaseHttpRequest {
    constructor() {
        this.options = {
            http2: true,
        };
    }
    prefixUrl(url) {
        this.options.prefixUrl = url;
        return this;
    }
    url(url) {
        this.options.url = url;
        return this;
    }
    method(method) {
        this.options.method = method;
        return this;
    }
    headers(headers) {
        var _a;
        this.options.headers = (_a = this.options.headers) !== null && _a !== void 0 ? _a : {};
        this.options.headers = {
            ...this.options.headers,
            ...headers,
        };
        return this;
    }
    bearerToken(bearerToken) {
        return this.headers({
            Authorization: `Bearer ${bearerToken}`,
        });
    }
    searchParams(searchParams) {
        this.options.searchParams = searchParams;
        return this;
    }
    async send() {
        let response;
        await (0, got_1.default)(this.options)
            .then((res) => {
            response = res;
        })
            .catch((error) => {
            if (error.response) {
                console.error('RESPONSE ERR', error.response.body);
                response = error.response;
            }
            else if (error.request) {
                // console.error('REQUEST ERR', error.request);
                response = error.request;
            }
            else {
                // console.error('Error', error.message);
                response = error.message;
            }
            //response = error;
        });
        return response;
    }
}
exports.BaseHttpRequest = BaseHttpRequest;
class ApiRequest extends BaseHttpRequest {
    constructor() {
        super();
        this.options = {
            ...this.options,
            responseType: 'json',
            hooks: {
                afterResponse: [
                    (response, retryWithMergedOptions) => {
                        var _a, _b, _c, _d, _e;
                        const stepName = `${response.statusCode} | ${(_b = (_a = this === null || this === void 0 ? void 0 : this.options) === null || _a === void 0 ? void 0 : _a.method) !== null && _b !== void 0 ? _b : 'GET'} ${(_c = this === null || this === void 0 ? void 0 : this.options) === null || _c === void 0 ? void 0 : _c.url} | ${(_e = (_d = response === null || response === void 0 ? void 0 : response.timings) === null || _d === void 0 ? void 0 : _d.phases) === null || _e === void 0 ? void 0 : _e.total}ms`;
                        // allure.createStep(stepName, () => {
                        //     if (this?.options?.json) {
                        //         allure.createAttachment(
                        //             `Request BODY`,
                        //             JSON.stringify(this?.options?.json, null, 2),
                        //             'application/json' as any
                        //         );
                        //     }
                        //     if (response.body) {
                        //         allure.createAttachment(
                        //             `Response BODY`,
                        //             JSON.stringify(response.body, null, 2),
                        //             'application/json' as any
                        //         );
                        //     }
                        // })();
                        return response;
                    },
                ],
            },
        };
        this.options.headers = {
            Accept: 'application/json',
        };
    }
    body(body) {
        this.options.json = body;
        return this;
    }
}
exports.ApiRequest = ApiRequest;
