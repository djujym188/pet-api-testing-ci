"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functionsForChecking_helper_1 = require("../../helpers/functionsForChecking.helper");
const student_controllers_1 = require("../lib/controllers/student.controllers");
const student = new student_controllers_1.StudentController();
describe('Student controller | with hooks', () => {
    let bearerToken;
    let userId;
    let id;
    before(`should get access token and userId`, async () => {
        let response = await student.postLogin();
        console.log(response.body);
        (0, functionsForChecking_helper_1.checkStatusCode)(response, 200);
        (0, functionsForChecking_helper_1.checkResponseTime)(response, 3000);
        bearerToken = response.body.accessToken;
        response = await student.getStudent(bearerToken);
        console.log(response.body);
        (0, functionsForChecking_helper_1.checkStatusCode)(response, 200);
        (0, functionsForChecking_helper_1.checkResponseTime)(response, 3000);
        id = response.body.Id;
        userId = response.body.userId;
    });
    it('test 3', async () => {
        let response = await student.postStudent(bearerToken, id, userId);
        console.log(response.body);
        (0, functionsForChecking_helper_1.checkStatusCode)(response, 200);
        (0, functionsForChecking_helper_1.checkResponseTime)(response, 3000);
    });
    it('test 4', async () => {
        let response = await student.getUserMe(bearerToken);
        console.log(response.body);
        (0, functionsForChecking_helper_1.checkStatusCode)(response, 200);
        (0, functionsForChecking_helper_1.checkResponseTime)(response, 3000);
    });
    it('test 5', async () => {
        let response = await student.getCourseAll(bearerToken);
        console.log(response.body);
        (0, functionsForChecking_helper_1.checkStatusCode)(response, 200);
        (0, functionsForChecking_helper_1.checkResponseTime)(response, 3000);
    });
    afterEach(function () {
        // runs after each test in this block
        console.log('It was a test');
    });
});
