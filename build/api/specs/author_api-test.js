"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functionsForChecking_helper_1 = require("../../helpers/functionsForChecking.helper");
const author_controller_1 = require("../lib/controllers/author.controller");
const author = new author_controller_1.AuthorController();
describe('Author controller | with hooks', () => {
    let bearerToken;
    let userId;
    let Id;
    let articleId;
    before(`should get access token and userId`, async () => {
        let response = await author.postLogin();
        console.log(response.body);
        (0, functionsForChecking_helper_1.checkStatusCode)(response, 200);
        (0, functionsForChecking_helper_1.checkResponseTime)(response, 3000);
        bearerToken = response.body.accessToken;
        response = await author.getAuthor(bearerToken);
        console.log(response.body);
        (0, functionsForChecking_helper_1.checkStatusCode)(response, 200);
        (0, functionsForChecking_helper_1.checkResponseTime)(response, 3000);
        Id = response.body.Id;
        userId = response.body.userId;
    });
    it('test 3', async () => {
        let response = await author.postAuthor(bearerToken, Id, userId);
        console.log(response.body);
        (0, functionsForChecking_helper_1.checkStatusCode)(response, 200);
        (0, functionsForChecking_helper_1.checkResponseTime)(response, 3000);
    });
    it('test 4', async () => {
        let response = await author.getUserMe(bearerToken);
        console.log(response.body);
        (0, functionsForChecking_helper_1.checkStatusCode)(response, 200);
        (0, functionsForChecking_helper_1.checkResponseTime)(response, 3000);
    });
    it('test 5', async () => {
        let response = await author.getAuthorOverviewId(bearerToken, Id);
        console.log(response.body);
        (0, functionsForChecking_helper_1.checkStatusCode)(response, 200);
        (0, functionsForChecking_helper_1.checkResponseTime)(response, 3000);
    });
    it('test 6', async () => {
        let response = await author.postArticle(bearerToken);
        console.log(response.body);
        (0, functionsForChecking_helper_1.checkStatusCode)(response, 200);
        (0, functionsForChecking_helper_1.checkResponseTime)(response, 3000);
        articleId = response.body.articleId;
    });
    it('test 7', async () => {
        let response = await author.getArticleAuthor(bearerToken);
        console.log(response.body);
        (0, functionsForChecking_helper_1.checkStatusCode)(response, 200);
        (0, functionsForChecking_helper_1.checkResponseTime)(response, 3000);
    });
    it('test 8', async () => {
        let response = await author.getArticleArticleId(bearerToken, articleId);
        console.log(response.body);
        (0, functionsForChecking_helper_1.checkStatusCode)(response, 200);
        (0, functionsForChecking_helper_1.checkResponseTime)(response, 3000);
    });
    it('test 10', async () => {
        let response = await author.postArticleComment(bearerToken, Id, articleId);
        console.log(response.body);
        (0, functionsForChecking_helper_1.checkStatusCode)(response, 200);
        (0, functionsForChecking_helper_1.checkResponseTime)(response, 3000);
    });
    afterEach(function () {
        // runs after each test in this block
        console.log('It was a test');
    });
});
