"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkResponseTime = exports.checkResponseBodyMessage = exports.checkResponseBodyStatus = exports.checkStatusCode = void 0;
const chai_1 = require("chai");
function checkStatusCode(response, statusCode) {
    (0, chai_1.expect)(response.statusCode, `Status Code should be ${statusCode}`).to.equal(statusCode);
}
exports.checkStatusCode = checkStatusCode;
function checkResponseBodyStatus(response, status) {
    (0, chai_1.expect)(response.body.status, `Status should be ${status}`).to.be.equal(status);
}
exports.checkResponseBodyStatus = checkResponseBodyStatus;
function checkResponseBodyMessage(response, message) {
    (0, chai_1.expect)(response.body.message, `Message should be ${message}`).to.be.equal(message);
}
exports.checkResponseBodyMessage = checkResponseBodyMessage;
function checkResponseTime(response, maxResponseTime = 3000) {
    (0, chai_1.expect)(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(maxResponseTime);
}
exports.checkResponseTime = checkResponseTime;
