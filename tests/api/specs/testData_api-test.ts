import {
    checkResponseTime,
    checkStatusCode,
    checkResponseBodyStatus,
    checkResponseBodyMessage,
} from '../../helpers/functionsForChecking.helper';
import { AuthorController } from '../lib/controllers/author.controller';
const author = new AuthorController();

describe('Use test data', () => {
    let invalidCredentialsDataSet = [
        { email: 'testcasesjs@gmail.com', password: '      ' },
        { email: 'testcases@gmail.com', password: 'weather18 ' },
        { email: 'testasesjs@gmail.com', password: 'weather 18' },
        { email: 'testcases@gmail.cm', password: 'admin' },
        { email: 'testcasesjs@gmail.com', password: 'testcasesjs@gmail.com' },
        { email: 'testcasesjs @ukr.net', password: 'weather18' },
        { email: 'testcasesjs@gmail.com  ', password: 'weather18' },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`login using invalid credentials : ${credentials.email} + ${credentials.password}`, async () => {
            let response = await author.postLogin();

            checkStatusCode(response, 401);
            checkResponseBodyStatus(response, 'UNAUTHORIZED');
            checkResponseBodyMessage(response, 'Bad credentials');
            checkResponseTime(response, 3000);
        });
    });


});