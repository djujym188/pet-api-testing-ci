import { expect } from "chai";
import { stringify } from "querystring";
import { checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { AuthorController} from "../lib/controllers/author.controller";
const author = new AuthorController();

describe('Author controller | with hooks', () => {
    let bearerToken: string;
    let userId: string;
    let Id: string;
    let articleId: string;

    before(`should get access token and userId`, async () => {
        let response = await author.postLogin()
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
        
        bearerToken = response.body.accessToken;
    
        response = await author.getAuthor(bearerToken)
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
      
        Id = response.body.Id;
        userId = response.body.userId;

        
    });

    it('test 3', async() =>{
        let response = await author.postAuthor(bearerToken, Id, userId );
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);       
    })


    it('test 4', async() =>{
        let response = await author.getUserMe(bearerToken);
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
    })


    it('test 5', async() =>{
        let response = await author.getAuthorOverviewId(bearerToken, Id)
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
    })

    it('test 6', async() =>{
        let response = await author.postArticle(bearerToken)
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
        
       articleId = response.body.articleId
    }) 

    it('test 7', async() =>{
        let response = await author.getArticleAuthor(bearerToken )
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
       
       
    })

    it('test 8', async() =>{

        let response = await author.getArticleArticleId(bearerToken,articleId )
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
       
       
    })

    
    it('test 10', async() =>{
        let response = await author.postArticleComment(bearerToken,Id, articleId)
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
        
       
    })

    afterEach(function () {
        // runs after each test in this block
        console.log('It was a test');
    });


})