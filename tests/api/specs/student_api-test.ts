import { expect } from "chai";
import { stringify } from "querystring";
import { checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { StudentController} from "../lib/controllers/student.controllers"
const student = new StudentController();

describe('Student controller | with hooks', () => {
    let bearerToken: string;
    let userId: string;
    let id: string;

    before(`should get access token and userId`, async () => {
        let response = await student.postLogin();
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
        
        bearerToken = response.body.accessToken;

        response = await student.getStudent(bearerToken);
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);

        id = response.body.Id;
        userId = response.body.userId;

        
    });

    it('test 3', async() =>{
        let response = await student.postStudent(bearerToken, id, userId );
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
    });


    it('test 4', async() =>{
        let response = await student.getUserMe(bearerToken);
        checkStatusCode(response, 200);
        checkResponseTime(response,3000); 
        
        
    })


    it('test 5', async() =>{
        let response = await student.getCourseAll(bearerToken);
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
       
    })

    afterEach(function () {
        // runs after each test in this block
        console.log('It was a test');
    });


})